# fs.rclonefs

rclonefs is a [PyFilesystem](https://github.com/PyFilesystem/pyfilesystem2)
implementation using [rclone](https://rclone.org). It acts as a bridge between
rclone and the API PyFilesystem provides, allowing easy use of storage within
Python that rclone can connect to. This is useful as rclone is a popular tool
which supports a wide range of cloud file storage providers.

## Installation

rclonefs requires Python 3.6 or later, and of course rclone. Rclone can be
installed with a package manager or downloaded directly
[from their website](https://rclone.org/downloads).

rclonefs can be installed using `pip`, which will automatically grab the
dependencies:

    pip install fs.rclonefs

## Usage

Once you've installed rclone and rclonefs, configure it to connect to your
cloud storage of choice. See rclone's
[config documentation](https://rclone.org/commands/rclone_config) for how to
do this.

Then, run rclone as a daemon to listen to remote control commands. This can be
done with the `rclone rcd` command, see the [rc documentation](https://rclone.org/rc)
for details and available options. The important options are `--rc-serve`,
which enables serving the files via HTTP, and optionally `--rc-no-auth` which
allows connecting without needing login credentials.

    rclone rcd --rc-serve

You will need the address and port rclone is using (localhost and 5572 by
default), and the name of your remote you want to use. If the daemon requires
authentication, you also need to provide a username and password. Then,
rclonefs is used in the same way as other fs filesystems.

### Examples

Some code examples are below (where *myremote* is a remote name).

Create with the constructor:

    from fs.rclonefs import RcloneFS
    rcfs = RcloneFS("myremote", username="myuser", password="mypass")

Or via an FS URL:

    import fs
    rcfs = fs.open_fs("rclone://myremote")

With credentials:

    rcfs = fs.open_fs("rclone://myuser:mypass@myremote")

Specifying a server address and port:

    rcfs = fs.open_fs("rclone://myremote?addr=192.168.1.2:1234")

Starting at a subdirectory:

    rcfs = fs.open_fs("rclone://myuser:mypass@myremote/path/to/dir")

See the [PyFilesystem](https://docs.pyfilesystem.org) documentation for more
details on how to construct and use a filesystem.

## Development

rclonefs is fully typed, which can be checked using [mypy](http://mypy-lang.org).
Packaging is done using [Poetry](https://python-poetry.org).

Feel free to create an issue with any bug reports or suggestions, or even merge
requests with fixes or new features.

## License

rclonefs is released under the
[Apache license, version 2.0](https://www.apache.org/licenses/LICENSE-2.0.html).
