# SPDX-License-Identifier: Apache-2.0
"""rclonefs.py

PyFilesystem implementation using Rclone. Acting as a bridge to add an Rclone
backend to Python, adds support for many cloud storage providers.

This file is part of fs.rclonefs. Copyright 2021 Oliver Galvin.
"""

import json
import os.path
from datetime import datetime
from logging import getLogger
from typing import TYPE_CHECKING, Any, BinaryIO, Collection, Iterator, List, Tuple

from fs.base import FS
from fs.enums import ResourceType
from fs.errors import (
    CreateFailed,
    DestinationExists,
    FileExpected,
    InvalidCharsInPath,
    NoURL,
    OperationFailed,
    PermissionDenied,
    ResourceNotFound,
)
from fs.info import Info
from fs.permissions import Permissions
from fs.subfs import SubFS
from requests import Session  # pylint: disable=wrong-import-order

try:
    from requests.exceptions import JSONDecodeError  # type: ignore
except ImportError:
    try:
        from simplejson.errors import JSONDecodeError  # type: ignore
    except ImportError:
        from json import JSONDecodeError

if TYPE_CHECKING:
    from fs.info import RawInfo

_log = getLogger("fs.rclonefs")


class RcloneFS(FS):
    """A filesystem to access cloud storage via rclone.

    Accesses an rclone server running at the address given, otherwise using
    rclone's default address and port. Credentials can be given using the
    `username` and `password` parameters, unless --rc-no-auth was used when
    starting the rclone rc daemon.

    Examples (where myremote is the remote name):
        Create with the constructor::

            >>> from fs.rclonefs import RcloneFS
            >>> rcfs = RcloneFS('myremote')

        Or via an FS URL::

            >>> import fs
            >>> rcfs = fs.open_fs('rclone://myremote')
    """

    def __init__(  # pylint: disable=too-many-arguments
        self,
        remote: str,
        address: str = None,
        port: int = None,
        username: str = None,
        password: str = None,
    ) -> None:
        self.session = Session()
        if username:
            self.session.auth = (username, password or "")

        self._address = address or "localhost"
        self._port = port or 5572

        super().__init__()

        try:
            self.remote = remote
        except ResourceNotFound as e:
            raise CreateFailed(exc=e) from e

    def __repr__(self) -> str:
        return f"RcloneFS({self.remote}, address={self._url()})"

    def __str__(self) -> str:
        return f"<rclonefs ({self.remote})>"

    @property
    def remote(self) -> str:
        """The current remote name.

        The remote is set by the constructor, and can be changed at any time.
        When set, the `fsinfo` is collected to fill in the `_meta` dict, which
        also checks the remote is valid; it exists and can be connected to.
        """
        return self._remote

    @remote.setter
    def remote(self, remote: str) -> None:
        if not remote.endswith(":"):
            remote += ":"
        _log.debug("Setting remote to %s", remote)

        self._fsinfo = self._run("operations/fsinfo", fs=remote)
        self._remote = remote
        _log.debug("fsinfo result: %s", self._fsinfo)

        # Fill in metadata
        #  Docs:
        #  https://rclone.org/overview/#restricted-filenames
        #  https://rclone.org/rc/#operations-fsinfo
        self._meta = {
            "case_insensitive": self._fsinfo["Features"]["CaseInsensitive"],
            "invalid_path_chars": "",  # rclone automatically converts invalid characters
            "max_path_length": None,
            "max_sys_path_length": None,
            "network": True,
            "read_only": False,
            "supports_rename": False,
        }

    def _url(self, path: str = None) -> str:
        url = f"http://{self._address}:{self._port}"
        if path is not None:
            url += f"/{path}"
        return url

    def _run(self, cmd: str, files=None, **kwargs: Any) -> Any:
        # API calls are always POST requests, and always return JSON
        # Docs: https://rclone.org/rc/#api-http
        with self._lock:
            res = self.session.post(self._url(cmd), files=files, data=kwargs)
        sc = res.status_code
        _log.debug("Command %s, with parameters %s, returned status code %i", cmd, kwargs, sc)
        try:
            dres = res.json()
        except JSONDecodeError as e:  # This should only happen if server isn't running
            _log.debug("Raw API response: %s", res.text)
            raise OperationFailed(
                msg=f"rclone API returned invalid JSON, with status code {sc}"
            ) from e
        if sc != 200:
            _log.debug("Raw API response: %s", dres)
            errmsg = dres.get("error")
            path = f"{kwargs['fs']}:" if "fs" in kwargs else ""
            path += kwargs["remote"] if "remote" in kwargs else ""
            if sc == 400:  # This would be an implementation error
                raise OperationFailed(msg=f"Bad request, '{errmsg}'")
            if sc == 403:
                raise PermissionDenied(
                    msg="Authentication failed - provide credentials or use --rc-no-auth"
                )
            if sc == 404:  # A remote, directory, or file was not found
                raise ResourceNotFound(path=path)
            if sc == 500:  # This code has several meanings, check the message
                if any(
                    x in errmsg for x in ("no such file", "didn't find section", "does not exist")
                ):
                    raise ResourceNotFound(path=path, msg=errmsg)
                if "invalid characters" in errmsg:
                    raise InvalidCharsInPath(path=path)
                if "not a regular file" in errmsg:
                    raise FileExpected(path=path)
            # Generic error fallback
            raise OperationFailed(msg=f"rclone API returned status code {sc}: '{errmsg}'")
        return dres

    def validatepath(self, path: str) -> str:
        # Rclone paths shouldn't start with a leading slash
        return super().validatepath(path).lstrip("/")

    def getinfo(self, path: str, namespaces: Collection[str] = None) -> Info:
        # We can't directly query a single file, so search its parent's children
        path = self.validatepath(path)
        parent, filename = os.path.split(path)
        for file_info in self.scandir(parent, namespaces=namespaces):
            if file_info.name == filename:
                return file_info
        raise ResourceNotFound(path=path)

    def scandir(
        self,
        path: str,
        namespaces: Collection[str] = None,
        page: Tuple[int, int] = None,
    ) -> Iterator[Info]:
        path = self.validatepath(path)
        kwargs = {"noMimeType": True}
        if namespaces is None:
            namespaces = {}
        if "details" not in namespaces:
            kwargs["noModTime"] = True
        if "file" in namespaces:
            kwargs["hash"] = True
        # Docs: https://rclone.org/commands/rclone_lsjson
        res = self._run("operations/list", fs=self.remote, remote=path, **kwargs)
        for item in res["list"]:
            raw_info = {
                "basic": {
                    "name": item["Name"],
                    "is_dir": item["IsDir"] or item.get("IsBucket"),
                }
            }
            if "details" in namespaces:
                raw_info.update(
                    {
                        "details": {
                            "accessed": None,  # not supported
                            "created": None,  # not supported
                            "metadata_changed": None,  # not supported
                            "modified": datetime.fromisoformat(item["ModTime"]),
                            "size": item["Size"],
                            "type": ResourceType.directory
                            if raw_info["is_dir"]
                            else ResourceType.file,
                        },
                    }
                )
            if "file" in namespaces:
                raw_info.update({"file": {"hashes": item["Hashes"]}})
            yield Info(raw_info)

    def listdir(self, path: str) -> List[str]:
        return [info.name for info in self.scandir(path)]

    def makedir(
        self,
        path: str,
        permissions: Permissions = None,
        recreate: bool = False,
    ) -> SubFS[FS]:
        path = self.validatepath(path)
        self._run("operations/mkdir", fs=self.remote, remote=path)
        return self.opendir(path)

    def openbin(
        self,
        path: str,
        mode: str = "r",
        buffering: int = -1,
        **options: Any,
    ) -> BinaryIO:
        pass  # TODO

    def remove(self, path: str) -> None:
        path = self.validatepath(path)
        self._run("operations/deletefile", fs=self.remote, remote=path)

    def removedir(self, path: str) -> None:
        path = self.validatepath(path)
        self._run("operations/rmdir", fs=self.remote, remote=path)

    def removetree(self, dir_path: str) -> None:
        dir_path = self.validatepath(dir_path)
        self._run("operations/purge", fs=self.remote, remote=dir_path)

    def setinfo(self, path: str, info: "RawInfo") -> None:
        pass  # TODO

    def geturl(self, path: str, purpose: str = "download") -> str:
        # This is an optional feature in rclone, and 'download' is the only
        # purpose relevant to PyFilesystem.
        # Docs: https://rclone.org/commands/rclone_link
        if purpose != "download" or not self._fsinfo["Features"].get("PublicLink"):
            raise NoURL(path, purpose)
        path = self.validatepath(path)
        res = self._run("operations/publicurl", fs=self.remote, remote=path)
        return res["url"]

    def mount(self, mount_point: str, read_only: bool = False, daemon: bool = False) -> None:
        """Mount the remote as a filesystem using FUSE.

        On Windows this depends on WinFSP being installed. When not mounting in
        daemon mode (i.e. when daemon = False), press Ctrl+C to stop the mount.

        Arguments:
            mount_point (str): A path on the local filesystem where the remote
                will be mounted.
            read_only (bool): If True, the mounted filesystem will be read only.
            daemon (bool): If True, the filesystem will mount in daemon (background)
                mode, and the function will return.

        Raises:
            fs.errors.ResourceNotFound: If ``path`` does not exist and
                ``mode`` does not imply creating the file, or if any
                ancestor of ``path`` does not exist.
        """
        mount_point = self.validatepath(mount_point)
        mount_options = {"ReadOnly": read_only, "Daemon": daemon}
        self._run(
            "mount/mount",
            fs=self.remote,
            mountPoint=mount_point,
            mountOpt=json.dumps(mount_options),
        )

    def upload(self, path: str, file: BinaryIO, chunk_size: int = None, **options: Any) -> None:
        path = self.validatepath(path)
        files = {os.path.basename(path): file}
        self._run("operations/uploadfile", files=files, fs=self._remote, remote=path)

    def download(self, path: str, file: BinaryIO, chunk_size: int = None, **options: Any) -> None:
        path = self.validatepath(path)
        chunk_size = 1024 * 1024 if chunk_size is None else chunk_size
        url = self._url(f"[{self.remote}:]/{path}")
        with self._lock:
            res = self.session.get(url, stream=True)
            if res.status_code != 200:
                raise ResourceNotFound(path=path)
            for chunk in res.iter_content(chunk_size=chunk_size):
                file.write(chunk)

    def move(
        self,
        src_path: str,
        dst_path: str,
        overwrite: bool = False,
        preserve_time: bool = False,  # pylint: disable=unused-argument
    ) -> None:
        # Rclone does a server-side move if possible
        src_path = self.validatepath(src_path)
        dst_path = self.validatepath(dst_path)
        if not overwrite and self.exists(dst_path):
            raise DestinationExists(dst_path)
        self._run(
            "operations/movefile",
            srcFs=self.remote,
            srcRemote=src_path,
            dstFs=self.remote,
            dstRemote=dst_path,
        )

    def copy(
        self,
        src_path: str,
        dst_path: str,
        overwrite: bool = False,
        preserve_time: bool = False,  # pylint: disable=unused-argument
    ) -> None:
        # Rclone does a server-side copy if possible
        src_path = self.validatepath(src_path)
        dst_path = self.validatepath(dst_path)
        if not overwrite and self.exists(dst_path):
            raise DestinationExists(dst_path)
        self._run(
            "operations/copyfile",
            srcFs=self.remote,
            srcRemote=src_path,
            dstFs=self.remote,
            dstRemote=dst_path,
        )
