# SPDX-License-Identifier: Apache-2.0
"""__init__.py

This file is part of fs.rclonefs. Copyright 2021 Oliver Galvin.
"""

__version__ = "0.1"
__author__ = "Oliver Galvin"
__author_email__ = "odg@riseup.net"
__license__ = "Apache-2.0"

from .opener import RcloneFSOpener
from .rclonefs import RcloneFS
