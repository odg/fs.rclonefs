"""opener.py

Opener for RCloneFS.

This file is part of fs.rclonefs. Copyright 2021 Oliver Galvin.
"""

__all__ = ["RcloneFSOpener"]

from fs.opener import Opener
from fs.opener.errors import OpenerError
from fs.opener.parse import ParseResult

from .rclonefs import RcloneFS


class RcloneFSOpener(Opener):  # pylint: disable=too-few-public-methods
    """Opener for RcloneFS.

    RcloneFS URLs are in the form `rclone://remote/path`, where `remote` is
    the remote name in Rclone's config. The only parameter is the optional
    `addr`, to specify the address and/or port to connect to, as specified in
    Rclone's `--rc-addr` command line option.
    """

    protocols = ["rclone"]

    @staticmethod
    def open_fs(
        fs_url: str,
        parse_result: ParseResult,
        writeable: bool,
        create: bool,
        cwd: str,
    ):  # pylint: disable=unused-argument
        remote, _, dir_path = parse_result.resource.partition("/")
        if not remote:
            raise OpenerError(f"invalid remote name in '{fs_url}'")
        address = parse_result.params.get("addr")
        try:
            address, port = address.split(":")
        except (ValueError, AttributeError):
            port = None
        fs = RcloneFS(
            remote,
            address=address,
            port=port,
            username=parse_result.username,
            password=parse_result.password,
        )
        if dir_path:
            return fs.opendir(dir_path)
        return fs
